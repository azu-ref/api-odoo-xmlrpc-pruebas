from xmlrpc import client

class OdooApi():
	def __init__(self, srv, port, db, user,pwd):
		common = client.ServerProxy('http://%s:%d/xmlrpc/2/common' % (srv, port))
		self.api = client.ServerProxy('http://%s:%d/xmlrpc/2/object' % (srv, port))
		self.uid = common.authenticate(db, user, pwd, {})
		self.pwd = pwd
		self.db = db
		self.model = 'crm.lead'
	def execute(self, method, arg_list, kwarg_dict=None):
		return self.api.execute_kw(self.db, self.uid, self.pwd, self.model, method, arg_list, kwarg_dict or {})

	def write(self, text, id=None):
		if id:
			self.execute('write', [[id], {'name': text}])
		else:
			vals = {'name': text, 'user_id': self.uid}
			id = self.execute('create', [vals])
		return id

	def read(self, ids=None):
		domain = [('id','in',ids)] if ids else []
		fields = ['id', 'name', 'is_done']
		return self.execute('search_read',[domain, fields])

if __name__ == '__main__':
	srv, port, db = '35.239.124.174',8069, '3mit'
	user, pwd = 'admin', 'Odoo'
	api = OdooApi(srv, port, db, user, pwd)
	api.write('gustavo test')
	from pprint import pprint
	pprint(api.read())

<!--<div class="col-auto mw-100 mb-2" t-if="o.date_due and o.type == 'out_invoice' and o.state in ('open', 'in_payment', 'paid')" name="due_date">-->
                        <!--      <strong>Due Date:</strong>-->
                        <!--      <p class="m-0" t-field="o.date_due"/>-->
                        <!--</div>-->

						<!--<div name="reference" class="col-auto mw-100 mb-2" t-if="o.reference">-->
                        <!--      <strong>Reference:</strong>-->
                        <!--      <p class="m-0" t-field="o.reference"/>-->
                        <!--</div>-->